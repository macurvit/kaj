MusiCalendar
=================

The place where you check for new music.


Requirements to run
-------------------

Latest browser (Chrome, Firefox, Vivaldi, ...)

Sass installed = [node (npm) installed](https://nodejs.org/en/download/), then run `npm install -g sass`

Installation
------------

1. Go to `PROJECT_ROOT/styles` and run `sass layout.sass:layout.css`
2. Open file `PROJECT_ROOT/index.html` in browser

Features
--------
- Display upcoming concerts and album releases (events) in form of a simple calendar
- Submit new events
- Find near concerts
- Offline detection to inform when it's unable to refresh list of events

Usage
-----
- Use mouse wheel or buttons aside to scroll through calendar
- Click on a date with any album or concert to display the events' details
- Use the "MENU" button (bottom right) to add a new event or browse nearest concerts
  - to add a new event, choose "Submit an event", pick either "Album" or "Concert" and fill in required data, then click "ADD EVENT"
  - to see nearest concerts, choose "Find nearest concerts" and enable location access in browser 
- with open modal, simply press `Esc` key to close it

Used technologies
-----------------
- HTML 5
- CSS 3 (SASS) with transitions and transforms
- JavaScript (ES6) with Geolocation API and Online API

Check for KAJ 
---------------

| Feature | Required | Done/Used | Max |
|:----------:|:-------------:|:------:|:---:|
| Documentation | Yes | Yes | 1 |
| Valid HTML5 | Yes | Yes | 1 |
| Functional in latest browser's versions | - | Yes | 2 |
| Semantic tags (section, article, ...) | Yes | Yes | 1 |
| Graphics (SVG, Canvas) | - | SVG (close modal) | 2 |
| Media (Audio, Video) | - | - | 2 |
| Form attributes | - | Yes | 2 |
| Offline application | - | Yes | 1 |
| CSS advanced selectors | Yes | Yes | 1 |
| Vendor prefixes | - | Yes (SASS mixin) | 1 |
| CSS3 2D/3D transforms | - | Yes | 2 |
| CSS3 Transitions/Animations | Yes | Yes | 2 |
| Media queries | - | - | 2 |
| JS OOP | Yes | Yes | 2 |
| JS framework or library | - | - | 1 |
| JS advanced APIs | Yes | Yes | 3 |
| JS Functional History | - | - | 2 |
| JS Media control | - | - | 1 |
| JS Offline application (API) | - | Yes | 1 |
| JS use of SVG | - | - | 2 |
| Overall result | - | - | 3 |
| Aesthetics | - | - | 2 |
| | | | 36 |


Implementation details (possible next updates)
----------------------
- With backend implementation use a XHR (or similar) request to fetch events for current month, not keep it in a simple variable
- Make the input for concert location a search bar for place (city), not specific coordinates
- With backend implementation (fetching events using XHR or similar) implement usage of localStorage to save the events to reduce the number of requests 
- Optimize writing events into calendar (currently, if there are many, all of them are rewritten on submit of a new one)
- Add an option to choose how many nearest concerts to see, to display only upcoming ones, not the ones that already happened, etc.
- Refresh events periodically so it is not necessary to reload the page
- Store more information about events (Description of an album - tracklist, album cover, etc.)
- Make the JS-generated HTML code prettier (use `\t \r \n` for correct indentation)
- Make the design prettier (it is prepared to make the change of colors easy in `PROJECT_ROOT/styles/_variables.sass`)
