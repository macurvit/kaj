// For shorter code
function getElem(elem) {
	return document.querySelector(elem);
}

function openMenu() {
	const content = `
	<div class="menu">
		<div class="submitEvent" id="eventLink">Submit an event</div>
		<div class="findConcerts" id="concertLink">Find nearest concerts</div>
	</div>
	`;
	createModal('Menu', content);
}

// Creates a modal message with given title and content
function createModal(title, content) {
	if (getElem('.overlay') !== undefined)
		closeModal();

	// Basic "template" for the modal message
	const html = `
<div class="overlay">
	<div class="modal">
		<div class="title">
			${title}
			<div class="close" id="closeModal">
				<svg xmlns="http://www.w3.org/2000/svg" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 30 30" xml:space="preserve" width="30px" height="30px">
					<path d="M25.707,6.293c-0.195-0.195-1.805-1.805-2-2c-0.391-0.391-1.024-0.391-1.414,0c-0.195,0.195-17.805,17.805-18,18  c-0.391,0.391-0.391,1.024,0,1.414c0.279,0.279,1.721,1.721,2,2c0.391,0.391,1.024,0.391,1.414,0c0.195-0.195,17.805-17.805,18-18  C26.098,7.317,26.098,6.683,25.707,6.293z"/>
					<path d="M23.707,25.707c0.195-0.195,1.805-1.805,2-2c0.391-0.391,0.391-1.024,0-1.414c-0.195-0.195-17.805-17.805-18-18  c-0.391-0.391-1.024-0.391-1.414,0c-0.279,0.279-1.721,1.721-2,2c-0.391,0.391-0.391,1.024,0,1.414c0.195,0.195,17.805,17.805,18,18  C22.683,26.098,23.317,26.098,23.707,25.707z"/>
				</svg>
			</div>
		</div>
		<div class="content">
			${content}
		</div>
	</div>
</div>`;


	getElem('body').insertAdjacentHTML('afterbegin', html);
	getElem('#closeModal').addEventListener('click', () => closeModal());

	// Bind necessary handlers based on modal content (either from to submit a new event or fetching concerts)
	if (getElem('#submitEvent')) {
		getElem('#submitEvent').addEventListener('submit', submitEvent);

		const modal = getElem('.modal');
		document.querySelectorAll('input[name="type"]').forEach((elem) => {
			elem.addEventListener('change', (e) => {
				if (getElem('input[name="type"]:checked').value === 'album') {
					modal.classList.remove('concert');
					document.querySelectorAll(['#latitude', '#longitude']).forEach(elem => elem.required = false);
					modal.classList.add('album');
				} else {
					modal.classList.remove('album');
					document.querySelectorAll(['#latitude', '#longitude']).forEach(elem => elem.required = true);
					modal.classList.add('concert');
				}
			})
		});
	}

	if (getElem('#eventLink'))
		getElem('#eventLink').addEventListener('click', () => {
			closeModal();
			createModal('Submit an event', createSubmitEventForm());
		});

	if (getElem('#concertLink'))
		getElem('#concertLink').addEventListener('click', () => {
			closeModal();
			createModal('Concerts around you', 'This feature needs access to your location, please allow the access.');
			getNearestConcerts();
		});

	// Prevent scrolling through calendar over modal since "Date overview" can be scrollable if there are many events
	window.removeEventListener('wheel', onWheel);

	// Accessibility
	document.addEventListener('keydown', (e) => {
		if (e.key === 'Escape') {
			closeModal();
		}
	})
}

function closeModal() {
	const overlay = getElem('.overlay');
	if (overlay !== null) {
		getElem('body').removeChild(overlay);
	}

	// Bind the wheel event back to enable scrolling through calendar
	window.addEventListener('wheel', onWheel);
}

// Display a day's events' details in modal
function showDateInfo(element) {
	let date = element.getAttribute('data-date');
	date = date.split('-');
	if (!((date[0] + '-' + date[1]) in events))
		return;

	const dateEvents = events[date[0] + '-' + date[1]][date.join('-')]; // Events that happen on this date
	const title = `${parseInt(date[2])}. ${parseInt(date[1])}. ${date[0]}`; // Only formatting to "d.m.Y"
	let content = '';

	if (dateEvents === undefined) // No events for given date => no need to continue
		return;

	if ('concerts' in dateEvents && dateEvents['concerts'].length > 0) { // First show concerts
		content += `<h3>Concerts</h3>
					<ul>`;
		dateEvents['concerts'].forEach(concert => {
			content += `<li>${concert.artist} - ${concert.name}</li>`;
		});
		content += `</ul>`;
	}

	if ('albums' in dateEvents && dateEvents['albums'].length > 0) { // Then show albums
		content += `<h3>Albums</h3>
					<ul>`;
		dateEvents['albums'].forEach(album => {
			const name = 'name' in album ? album.name : '<i>Unknown</i>';
			content += `<li>${album.artist} - ${album.name}</li>`;
		});
		content += `</ul>`;
	}

	// Display the result
	createModal(title, content);
}

// Handles adding of a new event
function submitEvent(e) {
	e.preventDefault();

	// Get references to inputs
	const date = getElem('#date');
	const artist = getElem('#artist');
	const name = getElem('#name');
	const concert = getElem('input[name="type"]:checked').value === 'concert';

	const eventDate = date.value.toString().split('-');
	const month = eventDate[0] + '-' + eventDate[1];

	if (!(month in events)) { // If there are no events for this month yet
		events[month] = {};
	}

	if (!(date.value in events[month])) { // If there are no events for this date yet
		events[month][date.value] = {
			'concerts': [],
			'albums': []
		};
	}

	if (concert) {
		const lat = getElem('#latitude');
		const long = getElem('#longitude');
		events[month][date.value]['concerts'].push({
			'artist': artist.value,
			'name': name.value,
			'latitude': parseFloat(lat.value),
			'longitude': parseFloat(long.value)
		});
	} else {
		events[month][date.value]['albums'].push({
			'artist': artist.value,
			'name': name.value
		})
	}

	writeEvents(calendar);
	closeModal();
}

// Write events to given calendar
function writeEvents(calendar) {
	const month = calendar.getYear() + '-' + calendar.getMonth().toString().padStart(2, '0');
	if (!(month in events)) // No events for given month => no need to continue
		return;

	Object.keys(events[month]).forEach((date) => {
		let year, month, day;
		[year, month, day] = date.split('-');

		const concerts = events[year + '-' + month][date].concerts;
		const albums = events[year + '-' + month][date].albums;
		let html = '';

		if (concerts.length)
			html += `<span class="concerts">${concerts.length} concert${concerts.length > 1 ? 's' : ''}</span>`;
		if (albums.length)
			html += `<span class="albums">${albums.length} album${albums.length > 1 ? 's' : ''}</span>`;

		const cell = getElem(`td[data-date="${date}"]`);
		cell.innerHTML = '<span>' + parseInt(day) + '</span>' + html;
	});
}

// Find events near current location
function getNearestConcerts() {
	// First get list of all concerts
	let concerts = Object.keys(events).flatMap((month) => {
		return Object.keys(events[month]).flatMap((date) => {
			events[month][date]['concerts'].forEach(concert => concert.date = date); // Need for duplicate checking
			return events[month][date]['concerts'];
		})
	});

	navigator.geolocation.getCurrentPosition((pos) => {
		const latitude = pos.coords.latitude;
		const longitude = pos.coords.longitude;

		// Compute distance of each concert
		concerts.forEach((concert) => {
			concert.distance = coordsToKmDistance(getCoordsDistance(concert, latitude, longitude));
		});

		// We don't want duplicate concerts to show (here duplicate = same artist, name and date)
		// This part for fetching distinct values is edited version of https://codeburst.io/javascript-array-distinct-5edc93501dc4
		const result = [];
		const map = new Map();
		for (const item of concerts) {
			if(!map.has(item.artist + '_' + item.name + '_' + item.date)){
				map.set(item.artist + '_' + item.name + '_' + item.date, true);    // set any value to Map
				result.push({
					id: item.id,
					date: item.date,
					artist: item.artist,
					name: item.name,
					latitude: item.latitude,
					longitude: item.longitude,
					distance: item.distance,
				});
			}
		}
		// End of copied part

		// Sort by distance from current position
		result.sort((a, b) => {
			if (a.distance < b.distance)
				return -1;
			else if (a.distance > b.distance)
				return 1;
			else
				return 0;
		});

		writeConcerts(result);
	}, console.debug); // Log for developers if something goes wrong with the position
}

// Display table with nearest concerts into current modal
function writeConcerts(concerts) {
	const content = getElem('.modal > .content');
	let html = `<table class="concerts">
					<tr>
						<th>Date</th>
						<th class="artist">Artist</th>
						<th>Name</th>
						<th class="distance">Distance</th>
					</tr>`;

	// Display only 5 nearest concerts
	const top = concerts.slice(0, 5);
	top.forEach((concert) => {
		const dateParts = concert.date.split('-');
		const date = parseInt(dateParts[2]) + '. ' + parseInt(dateParts[1]) +'. ' + dateParts[0];
		html += `<tr><td>${date}</td><td class="artist">${concert.artist}</td><td>${concert.name}</td><td class="distance">${concert.distance} km</td></tr>`;
	});
	html += `</html>`;
	content.innerHTML = '';
	content.insertAdjacentHTML('afterbegin', html);
}

// Show info if client is offline
function updateStatus() {
	const statusBar = getElem('.online_status');
	if (!navigator.onLine) {
		statusBar.classList.add('show');
	} else {
		statusBar.classList.remove('show');
	}
}

// Return form for submitting new event
function createSubmitEventForm() {
	return `<form method="post" id="submitEvent" action="./index.html">
		<div class="inputs">
			<div class="radio_buttons">
				<input type="radio" name="type" value="album" id="album" checked><label for="album">Album</label>
				<input type="radio" name="type" value="concert" id="concert"><label for="concert">Concert</label>
			</div>
			<div class="input_group">
				<label for="artist">Artist</label><input type="text" name="artist" id="artist" placeholder="Imagine Dragons" autofocus required />
			</div>
			<div class="input_group">
				<label for="name">Name</label><input type="text" name="name" id="name" placeholder="Origins"/>
			</div>
			<div class="input_group">
				<label for="date">Date</label><input type="date" name="date" id="date" required/>
			</div>
			<div class="concert">
				<div class="input_group">
					<label for="latitude">Latitude</label><input type="text" name="latitude" id="latitude" placeholder="50.635" />
				</div>
				<div class="input_group">
					<label for="longitude">Longitude</label><input type="text" name="longitude" id="longitude" placeholder="17.41" />
				</div>
			</div>
			<input type="submit" value="Add event" />
		</div>
		</form>`;
}

// Compute distance of event from given latitude and longitude (in "degrees")
function getCoordsDistance(event, lat, long) {
	return Math.sqrt(Math.pow(lat - event.latitude, 2) + Math.pow(long - event.longitude, 2));
}

// Convert "degrees distance" to real distance, number taken from https://sciencing.com/convert-distances-degrees-meters-7858322.html
function coordsToKmDistance(coordsDistance) {
	return (coordsDistance * 111139 / 1000).toFixed(1);
}

// Scrolling through calendar on mouse wheel
function onWheel(e) {
	e.deltaY > 0 ? calendar.nextMonth() : calendar.previousMonth();
}