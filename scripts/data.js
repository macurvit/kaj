const events = {
	"2019-06": {
		"2019-06-07": {
			"albums": [
				{
					"artist": "Imagine Dragons",
					"name": "Night Visions",
					"image": ""
				}
			],
			"concerts": [],
		},
		"2019-06-05": {
			"albums": [
				{
					"artist": "A good one",
					"name": "ULTRA SUPER ALBUM",
				},
				{
					"artist": "Not so good one",
					"name": "NE TAK SUPER ALBUM",
				},
			],
			"concerts": []
		},
		"2019-06-30": {
			"albums": [],
			"concerts": [ // So many concerts just to test overflow - should be scrollable
				{
					"latitude": 49.1667,
					"longitude": 16.95,
					"artist": "Somebody",
					"name": "KONCERT - SKUPINA",
					"image": "..."
				},
				{
					"latitude": 52.123412,
					"longitude": 16.4124512,
					"artist": "Someone",
					"name": "KONCERT - SKUPINA",
					"image": "..."
				},
				{
					"latitude": 47.123412,
					"longitude": 16.4124512,
					"artist": "Nobody",
					"name": "KONCERT - SKUPINA",
					"image": "..."
				},
				{
					"latitude": 50.123412,
					"longitude": 16.4124512,
					"artist": "None",
					"name": "KONCERT - SKUPINA",
					"image": "..."
				},
				{
					"latitude": 50.123412,
					"longitude": 16.4124512,
					"artist": "None",
					"name": "KONCERT - SKUPINA",
					"image": "..."
				},
				{
					"latitude": 50.123412,
					"longitude": 16.4124512,
					"artist": "None",
					"name": "KONCERT - SKUPINA",
					"image": "..."
				},
				{
					"latitude": 50.123412,
					"longitude": 16.4124512,
					"artist": "None",
					"name": "KONCERT - SKUPINA",
					"image": "..."
				},
				{
					"latitude": 50.123412,
					"longitude": 16.4124512,
					"artist": "None",
					"name": "KONCERT - SKUPINA",
					"image": "..."
				},
				{
					"latitude": 50.123412,
					"longitude": 16.4124512,
					"artist": "None",
					"name": "KONCERT - SKUPINA",
					"image": "..."
				},
				{
					"latitude": 50.123412,
					"longitude": 16.4124512,
					"artist": "None",
					"name": "KONCERT - SKUPINA",
					"image": "..."
				},
				{
					"latitude": 50.123412,
					"longitude": 16.4124512,
					"artist": "None",
					"name": "KONCERT - SKUPINA",
					"image": "..."
				},
				{
					"latitude": 50.123412,
					"longitude": 16.4124512,
					"artist": "None",
					"name": "KONCERT - SKUPINA",
					"image": "..."
				},
				{
					"latitude": 50.123412,
					"longitude": 16.4124512,
					"artist": "None",
					"name": "KONCERT - SKUPINA",
					"image": "..."
				},
				{
					"latitude": 50.123412,
					"longitude": 16.4124512,
					"artist": "None",
					"name": "KONCERT - SKUPINA",
					"image": "..."
				},
				{
					"latitude": 50.123412,
					"longitude": 16.4124512,
					"artist": "None",
					"name": "KONCERT - SKUPINA",
					"image": "..."
				},
				{
					"latitude": 50.123412,
					"longitude": 16.4124512,
					"artist": "None",
					"name": "KONCERT - SKUPINA",
					"image": "..."
				},
				{
					"latitude": 50.123412,
					"longitude": 16.4124512,
					"artist": "None",
					"name": "KONCERT - SKUPINA",
					"image": "..."
				},
				{
					"latitude": 50.123412,
					"longitude": 16.4124512,
					"artist": "None",
					"name": "KONCERT - SKUPINA",
					"image": "..."
				},
				{
					"latitude": 50.123412,
					"longitude": 16.4124512,
					"artist": "None",
					"name": "KONCERT - SKUPINA",
					"image": "..."
				},
				{
					"latitude": 50.123412,
					"longitude": 16.4124512,
					"artist": "None",
					"name": "KONCERT - SKUPINA",
					"image": "..."
				},
				{
					"latitude": 50.123412,
					"longitude": 16.4124512,
					"artist": "None",
					"name": "KONCERT - SKUPINA",
					"image": "..."
				},
				{
					"latitude": 50.123412,
					"longitude": 16.4124512,
					"artist": "None",
					"name": "KONCERT - SKUPINA",
					"image": "..."
				},
				{
					"latitude": 50.123412,
					"longitude": 16.4124512,
					"artist": "None",
					"name": "KONCERT - SKUPINA",
					"image": "..."
				},
				{
					"latitude": 50.123412,
					"longitude": 16.4124512,
					"artist": "None",
					"name": "KONCERT - SKUPINA",
					"image": "..."
				},
				{
					"latitude": 50.123412,
					"longitude": 16.4124512,
					"artist": "None",
					"name": "KONCERT - SKUPINA",
					"image": "..."
				},
				{
					"latitude": 50.123412,
					"longitude": 16.4124512,
					"artist": "None",
					"name": "KONCERT - SKUPINA",
					"image": "..."
				}
			],
		},
	}
};