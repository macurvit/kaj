// Date = num of day in month (0 - 30)
// Day = num of day in week (0 - 6); 0 = Sunday in Date()
class Calendar
{
	constructor(id)
	{
		this.now = new Date();
		this.element = document.querySelector(id);
		this.year = this.now.getFullYear();
		this.month = this.now.getMonth(); // Indexing from 0 to 11 = January to December
		this.monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
	}

	render()
	{
		const currentMonthFirstDay = new Date(this.year, this.month, 1).getDay(); // Number of day in week, !! starts with Sunday = 0
		const currentMonthLastDate = new Date(this.year, this.month + 1, 0).getDate();

		let previousMonthLastDate;

		if (this.month === 0) {
			previousMonthLastDate = new Date(this.year - 1, 11, 0).getDate();
		} else {
			previousMonthLastDate = new Date(this.year, this.month, 0).getDate();
		}

		let html = '<table class="calendar">';

		let day = 1; // Keeps number of current day in month
		let diff = currentMonthFirstDay === 0 ? 6 : (currentMonthFirstDay - 1);
		diff = -diff;

		// Up to 6 rows - for months starting on Saturday/Sunday and having 31/30 days
		for (let row = 0; row < 6; row++) {
			html += '<tr class="inserting">';

			// 7 days of week => 7 columns
			for (let column = 0; column < 7; column++) {

				if (diff < 0) { // Previous month's days
					html += `<td class="prev">${previousMonthLastDate + diff + 1}</td>`; // No of previous last day + No of previous days left + from 0
					diff++;
				} else if (diff >= currentMonthLastDate) { // Next month's days
					if (row === 5 && column > 4) {
						html += `<td colspan="2" class="info">${this.monthNames[this.month]} ${this.year}<br>MENU</td>`;
						column++;
					}
					else
						html += `<td class="next">${++diff - currentMonthLastDate}</td>`;
				} else { // Current month's days
					const dataDate = this.year + "-" + (this.month + 1).toString().padStart(2, 0) + "-" + day.toString().padStart(2, 0);
					if (this.year === this.now.getFullYear() && this.month === this.now.getMonth() && day === this.now.getDate()) {
						html += `<td data-date="${dataDate}" class="today current_month" tabindex="${day + 2}"><span>${day++}</span></td>`;
					} else {
						html += `<td data-date="${dataDate}" class="current_month" tabindex="${day + 2}"><span>${day++}</span><span></span></td>`;
					}
					diff++
				}
			}
			html += '</tr>';
		}

		html += '</table>';

		this.element.innerHTML = html;

		setTimeout(() => {
			document.querySelectorAll(".inserting").forEach((elem) => {
				elem.classList.remove("inserting");
			})
		}, 300);

		writeEvents(this);

		getElem('.info').addEventListener('click', () => openMenu());

		document.querySelectorAll('td.current_month').forEach(elem => {
			elem.addEventListener('click', e => showDateInfo(e.currentTarget));
			elem.addEventListener('keydown', e => {
				if (e.key === 'Enter') {
					showDateInfo(e.currentTarget);
				}
			});
		});
	}

	nextMonth() {
		this.month = this.month === 11 ? 0 : (this.month + 1);

		// We set it to January = next year
		if (this.month === 0)
			this.year++;

		this.render();
	}

	previousMonth() {
		this.month = this.month === 0 ? 11 : (this.month - 1);

		// We set it to December = prev year
		if (this.month === 11)
			this.year--;

		this.render();
	};

	getMonth() {
		return this.month + 1;
	}

	getYear() {
		return this.year;
	}
}